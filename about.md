[porkbun-gui-rs](https://git.envs.net/iacore/porkbun-gui-rs)  [AGPL v3](https://www.gnu.org/licenses/agpl-3.0.txt)  Copyright (C) 2022-2023  Locria Cyber

This software uses Porkbun's HTTP API to help you manage your DNS records on Porkbun.

Every relevant button click sends an HTTP request, so **do not spam click**.

## Environment Variables

Set these before you open this application, so you don't have to fill out the **Credentials** every time.

**PORKBUN_PUBLIC**: API Key. Get yours at [https://porkbun.com/account/api](https://porkbun.com/account/api)  
**PORKBUN_PRIVATE**: API Secret  
**PORKBUN_DOMAIN**: Default domain name
