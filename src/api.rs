use crate::prelude::*;
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use serde_with::{serde_as, DisplayFromStr};

#[cfg(feature = "mock")]
mod mock;

pub const PORKBUN_API_PREFIX: &str = "https://api.porkbun.com/api/json/v3/dns";

pub struct HttpClient {
    #[allow(clippy::type_complexity)]
    http_post: fn(url: &str, payload: &[u8]) -> FetchResult<Box<dyn std::io::Read + Send + Sync>>,
}

impl Default for HttpClient {
    #[cfg(not(feature = "mock"))]
    fn default() -> Self {
        fn http_post(
            url: &str,
            payload: &[u8],
        ) -> FetchResult<Box<dyn std::io::Read + Send + Sync>> {
            let send_bytes = match ureq::post(url).send_bytes(payload) {
                Ok(a) => a,
                Err(err) => match err {
                    ureq::Error::Status(status_code, response) => {
                        let mut buf = String::new();
                        response.into_reader().read_to_string(&mut buf).unwrap();
                        println!("{}", buf);
                        return Err(FetchError::Status(status_code));
                    }
                    _ => return Err(err.into()),
                },
            };
            Ok(send_bytes.into_reader())
        }
        Self { http_post }
    }

    #[cfg(feature = "mock")]
    fn default() -> Self {
        Self {
            http_post: mock::http_post_mock,
        }
    }
}

#[serde_as]
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct DnsRecord {
    #[serde_as(as = "DisplayFromStr")]
    pub id: u32,
    pub name: Option<String>,
    #[serde(rename = "type")]
    pub type_: String,
    pub content: String,
    #[serde_as(as = "Option<DisplayFromStr>")]
    pub ttl: Option<i32>,
    #[serde_as(as = "Option<DisplayFromStr>")]
    pub prio: Option<i32>,
    pub notes: Option<String>,
}

impl Default for DnsRecord {
    fn default() -> Self {
        Self {
            id: 0,
            name: Some("".into()),
            type_: "A".into(),
            content: "127.0.0.1".into(),
            ttl: None,
            prio: None,
            notes: None,
        }
    }
}

#[derive(Deserialize, Debug)]
pub struct ReadAllResponse {
    pub records: Vec<DnsRecord>,
}

#[derive(Deserialize, Debug)]
pub struct CreateResponse {
    pub id: u32,
}

#[derive(Deserialize)]
pub struct EditResponse {}

#[derive(Deserialize)]
pub struct DeleteResponse {}

pub type FnStartRequest<T> = Box<dyn FnOnce() -> FetchResult<T> + Send>;

impl Globals {
    fn send_request<Se, De>(&self, url: String, payload: &Se) -> FnStartRequest<De>
    where
        Se: Serialize,
        De: DeserializeOwned,
    {
        let payload = serde_json::to_vec(payload).unwrap();
        let http_client = self.http_client.clone();
        Box::new(move || {
            let resp_body = (http_client.http_post)(&url, &payload)?;
            let value: serde_json::Value = serde_json::from_reader(resp_body)?;
            if value["status"] != "SUCCESS" {
                return Err(FetchError::Application(value));
            }
            Ok(serde_json::from_value(value)?)
        })
    }

    pub fn read_all(&self) -> FnStartRequest<ReadAllResponse> {
        let url = format!("{PORKBUN_API_PREFIX}/retrieve/{}", self.domain);
        let f: FnStartRequest<ReadAllResponse> = self.send_request(url, &self.creds);
        let domain = self.domain.clone();
        Box::new(move || match f() {
            Ok(mut k) => {
                for record in &mut k.records {
                    if let Some(name) = &mut record.name {
                        if let Some(stripped) = name.strip_suffix(&format!(".{domain}")) {
                            *name = stripped.into();
                        } else if name == &domain {
                            *name = "".into();
                        } else {
                            panic!("What DNS record key is this? name={name:?} domain={domain:?}")
                        }
                    }
                }
                Ok(k)
            }
            Err(e) => Err(e),
        })
    }

    pub fn create(&self, record: &DnsRecord) -> FnStartRequest<CreateResponse> {
        let url = format!("{PORKBUN_API_PREFIX}/create/{}", self.domain);
        self.send_request(url, &to_create_or_edit_payload(&self.creds, record))
    }

    pub fn edit(&self, record: &DnsRecord) -> FnStartRequest<EditResponse> {
        let url = format!("{PORKBUN_API_PREFIX}/edit/{}/{}", self.domain, record.id);
        self.send_request(url, &to_create_or_edit_payload(&self.creds, record))
    }

    pub fn delete(&self, record_id: u32) -> FnStartRequest<DeleteResponse> {
        let url = format!("{PORKBUN_API_PREFIX}/delete/{}/{}", self.domain, record_id);
        self.send_request(url, &self.creds)
    }
}

fn to_create_or_edit_payload(creds: &Credentials, record: &DnsRecord) -> serde_json::Value {
    use serde_json::to_value;
    let mut value = to_value(creds).unwrap();
    value["name"] = to_value(&record.name).unwrap();
    value["type"] = to_value(&record.type_).unwrap();
    value["content"] = to_value(&record.content).unwrap();
    value["ttl"] = to_value(record.ttl).unwrap();
    value["prio"] = to_value(record.prio).unwrap();
    value["notes"] = to_value(&record.notes).unwrap();
    value
}
