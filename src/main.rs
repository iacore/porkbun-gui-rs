mod api;
mod prelude;
mod windows;

use crate::prelude::*;
use eframe::{egui, epaint::Color32};
use egui_commonmark::CommonMarkViewer;

fn main() -> Result<(), eframe::Error> {
    #[cfg(feature = "mock")]
    println!("!!Mock backend!!");
    eframe::run_native(
        "Porkbun DNS Manager",
        eframe::NativeOptions::default(),
        Box::new(|_cc| Box::<RootWindow>::default()),
    )
}

#[derive(Default)]
struct RootWindow {
    globals: Globals,
    // windows: Vec<util::Task<Result<String, ureq::Error>>>,
    windows: Vec<Box<dyn TopLevelWindow>>,
}

impl eframe::App for RootWindow {
    fn update(&mut self, ctx: &egui::Context, frame: &mut eframe::Frame) {
        let mut commands = CommandQueue::new();

        // show all windows
        for window in &mut self.windows {
            commands.append(&mut window.show(&mut self.globals, ctx, frame));
        }
        commands.append(&mut Self::show(&mut self.globals, ctx, frame));

        self.windows.retain_mut(|w| w.open());
        for cmd in commands {
            match cmd {
                AppCommand::CreateWindow(win) => self.windows.push(win),
            }
        }
    }
}

impl RootWindow {
    fn show(g: &mut Globals, ctx: &egui::Context, _frame: &mut eframe::Frame) -> CommandQueue {
        let mut commands = CommandQueue::new();
        egui::TopBottomPanel::top("root_top").show(ctx, |ui| {
            ui.horizontal(|ui| {
                Self::show_actions(g, ui, &mut commands);
            });
        });
        egui::CentralPanel::default().show(ctx, |ui| {
            egui::ScrollArea::vertical().show(ui, |ui| {
                let has_valid_config = g.validate_config();
                if let Err(e) = has_valid_config {
                    ui.label("Editing functionality is disabled due to invalid configuration.");
                    ui.colored_label(Color32::YELLOW, e);
                }

                Self::show_options(g, ui);
                Self::show_usage(g, ui);
            });
        });
        commands
    }

    fn show_actions(g: &mut Globals, ui: &mut egui::Ui, commands: &mut CommandQueue) {
        let has_valid_config = g.validate_config();
        ui.add_enabled_ui(has_valid_config == Ok(()), |ui| {
            if ui.button("Fetch All Existing Records").clicked() {
                commands.create_window(ReadAllWindow::new(g.next_win_id(), g));
            }

            if ui.button("Create A New Record").clicked() {
                commands.create_window(CreateWindow::new(g.next_win_id()))
            }
        });
    }

    fn show_options(g: &mut Globals, ui: &mut egui::Ui) {
        egui::CollapsingHeader::new("Configuration").show(ui, |ui| {
            egui::Grid::new("cred settings")
                .num_columns(2)
                .show(ui, |ui| {
                    labeled_text_edit(ui, "Domain Name", &mut g.domain, true);
                    ui.end_row();
                    labeled_text_edit(ui, "API Key", &mut g.creds.public, g.show_creds);
                    ui.end_row();
                    labeled_text_edit(ui, "API Secret", &mut g.creds.secret, g.show_creds);
                    ui.end_row();
                    ui.checkbox(&mut g.show_creds, "Show Credentials");
                    ui.end_row();
                });
        });
    }

    fn show_usage(g: &mut Globals, ui: &mut egui::Ui) {
        egui::CollapsingHeader::new("About and Usage").show(ui, |ui| {
            CommonMarkViewer::new("viewer").show(ui, &mut g.md_cache, include_str!("../about.md"));
        });
    }
}
