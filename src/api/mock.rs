use crate::prelude::*;

struct StringReader<'a> {
    inner: &'a str,
    pos: usize,
}

impl<'a> StringReader<'a> {
    fn new(inner: &'a str) -> Self {
        Self { inner, pos: 0 }
    }
}

impl std::io::Read for StringReader<'_> {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        let slice = self.inner.as_bytes();
        let remaining_len = slice.len() - self.pos;
        let read_how_much = usize::min(remaining_len, buf.len());
        for i in 0..read_how_much {
            buf[i] = slice[self.pos + i];
        }
        self.pos += read_how_much;
        Ok(read_how_much)
    }
}

pub fn http_post_mock(
    url: &str,
    _payload: &[u8],
) -> FetchResult<Box<dyn std::io::Read + Send + Sync>> {
    assert!(url.starts_with(PORKBUN_API_PREFIX));
    let url = &url[PORKBUN_API_PREFIX.len()..];

    let mock_response = if url.starts_with("/retrieve/") {
        r#"{"status":"SUCCESS","cloudflare":"enabled","records":[
            {"id":"1234","name":"www.example.com","type":"A","content":"127.0.0.1","ttl":"600","prio":"0","notes":""},
            {"id":"1235","name":"www.example.com","type":"A","content":"127.0.0.1","ttl":"600","prio":"0","notes":""},
            {"id":"1236","name":"www.example.com","type":"A","content":"127.0.0.1","ttl":"600","prio":"0","notes":""},
            {"id":"1237","name":"www.example.com","type":"A","content":"127.0.0.1","ttl":"600","prio":"0","notes":""},
            {"id":"1238","name":"www.example.com","type":"A","content":"127.0.0.1","ttl":"600","prio":"0","notes":""}
        ]}"#
    } else if url.starts_with("/create/") {
        r#"{"status":"SUCCESS","id":287942785}"#
    } else if url.starts_with("/edit/") {
        r#"{"status":"SUCCESS"}"#
    } else if url.starts_with("/delete/") {
        r#"{"status":"SUCCESS"}"#
    } else {
        eprintln!("Unmocked URL: {}", url);
        return Err(FetchError::Status(500));
    };
    return Ok(Box::new(StringReader::new(mock_response)));
}
