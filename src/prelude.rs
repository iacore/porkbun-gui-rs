use std::collections::LinkedList;
use std::mem::{self, MaybeUninit};
use std::sync::Arc;
use std::thread::JoinHandle;

use eframe::egui;
use egui_commonmark::CommonMarkCache;
use serde::Serialize;

pub use crate::api::*;
pub use crate::windows::*;

#[derive(Serialize)]
pub struct Credentials {
    #[serde(rename = "apikey")]
    pub public: String,
    #[serde(rename = "secretapikey")]
    pub secret: String,
}

pub struct Globals {
    // internal state
    pub next_win_id: u16,
    pub md_cache: CommonMarkCache,
    pub http_client: Arc<HttpClient>,
    
    // configuration
    pub show_creds: bool,
    pub creds: Credentials,
    pub domain: String,
}

impl Default for Globals {
    fn default() -> Self {
        Self {
            next_win_id: 0,
            show_creds: false,
            domain: env_var("PORKBUN_DOMAIN").unwrap_or_else(|| "".into()),
            creds: Credentials {
                public: env_var("PORKBUN_PUBLIC").unwrap_or_else(|| "".into()),
                secret: env_var("PORKBUN_SECRET").unwrap_or_else(|| "".into()),
            },
            md_cache: Default::default(),
            http_client: Arc::new(HttpClient::default()),
        }
    }
}

impl Globals {
    pub fn validate_config(&self) -> Result<(), String> {
        if self.domain.is_empty() {
            return Err("Domain Name cannot be empty".into());
        }
        // idea: verify domain name is actually domain name
        if self.creds.public.is_empty() {
            return Err("API public key cannot be empty".into());
        }
        if self.creds.secret.is_empty() {
            return Err("API secret key cannot be empty".into());
        }
        Ok(())
    }

    pub fn next_win_id(&mut self) -> u16 {
        let r = self.next_win_id;
        self.next_win_id += 1;
        r
    }
}

pub enum AppCommand {
    CreateWindow(Box<dyn TopLevelWindow>),
}

pub type CommandQueue = LinkedList<AppCommand>;

pub trait TopLevelWindow {
    fn show(
        &mut self,
        g: &mut Globals,
        ctx: &egui::Context,
        frame: &mut eframe::Frame,
    ) -> CommandQueue;
    /// return `false` to release resource
    fn open(&self) -> bool;
}

pub trait CommandQueueExt {
    fn create_window<W>(&mut self, window: W)
    where
        W: TopLevelWindow + 'static;
}

impl CommandQueueExt for CommandQueue {
    fn create_window<W>(&mut self, window: W)
    where
        W: TopLevelWindow + 'static,
    {
        self.push_back(AppCommand::CreateWindow(Box::new(window)))
    }
}

pub enum Task<T> {
    Running(MaybeUninit<JoinHandle<T>>),
    Done(T),
}

impl<T> From<JoinHandle<T>> for Task<T> {
    fn from(value: JoinHandle<T>) -> Self {
        Self::Running(MaybeUninit::new(value))
    }
}

impl<T> Task<T>
where
    T: Send + 'static,
{
    pub fn poll(&mut self) -> &Self {
        if let Self::Running(join_handle) = self {
            let join_handle = unsafe { join_handle.assume_init_read() };
            if join_handle.is_finished() {
                let thread_result = match join_handle.join() {
                    Ok(k) => k,
                    Err(e) => std::panic::resume_unwind(e),
                };
                *self = Self::Done(thread_result)
            } else {
                mem::forget(join_handle);
            }
        }
        self
    }
    pub fn spawn<F>(f: F) -> Task<T>
    where
        F: FnOnce() -> T,
        F: Send + 'static,
    {
        std::thread::spawn(f).into()
    }
}

impl<T> Task<FetchResult<T>>
where
    T: Send + 'static,
{
    pub fn show_if_success<F>(&mut self, ui: &mut egui::Ui, f: F)
    where
        F: FnOnce(&mut egui::Ui, &T),
    {
        match self.poll() {
            Task::Running(_) => {
                ui.label("Loading");
            }
            Task::Done(resp) => match resp {
                Ok(res) => f(ui, res),
                Err(err) => {
                    ui.label("Error:");
                    ui.label(err.to_string());
                    // todo: show different error differently
                }
            },
        }
    }
}

pub fn env_var(key: &str) -> Option<String> {
    std::env::var_os(key).map(|x| -> String { x.to_string_lossy().into() })
}

// ui

pub fn labeled_text_edit(
    ui: &mut egui::Ui,
    label: &str,
    edit_buffer: &mut String,
    show_clear_text: bool,
) -> egui::Response {
    let label = ui.label(label);

    egui::TextEdit::singleline(edit_buffer)
        .password(!show_clear_text)
        .show(ui)
        .response
        .labelled_by(label.id)
}

pub fn toggleable_label<T>(
    ui: &mut egui::Ui,
    text: &str,
    mut_ref: &mut Option<T>,
    default: impl Fn() -> T,
) -> egui::Response {
    let l = ui
        .vertical_centered(|ui| ui.selectable_label(mut_ref.is_some(), text))
        .inner;
    if l.clicked() {
        if mut_ref.is_some() {
            *mut_ref = None;
        } else {
            *mut_ref = Some(default());
        }
    }
    l
}

pub fn record_editor(ui: &mut egui::Ui, record: &mut DnsRecord) {
    ui.label("Some fields are optional. To toggle a field, click on its name.");
    egui::Grid::new("").num_columns(2).show(ui, |ui| {
        let l = toggleable_label(ui, "name", &mut record.name, String::new);

        if let Some(value) = &mut record.name {
            ui.text_edit_singleline(value).labelled_by(l.id);
        }
        ui.end_row();

        let l = ui.label("type");
        ui.vertical_centered(|ui| ui.text_edit_singleline(&mut record.type_).labelled_by(l.id));
        ui.end_row();

        let l = ui.label("content");
        ui.vertical_centered(|ui| {
            ui.text_edit_singleline(&mut record.content)
                .labelled_by(l.id)
        });
        ui.end_row();

        let l = toggleable_label(ui, "ttl", &mut record.ttl, || 600);
        if let Some(value) = &mut record.ttl {
            ui.add(egui::DragValue::new(value)).labelled_by(l.id);
        }
        ui.end_row();

        let l = toggleable_label(ui, "prio", &mut record.prio, || 0);
        if let Some(value) = &mut record.prio {
            ui.add(egui::DragValue::new(value)).labelled_by(l.id);
        }
        ui.end_row();

        let l = toggleable_label(ui, "notes", &mut record.notes, String::new);
        if let Some(value) = &mut record.notes {
            ui.text_edit_singleline(value).labelled_by(l.id);
        }
        ui.end_row();
    });
}

#[derive(thiserror::Error, Debug)]
pub enum FetchError {
    #[error("HTTP status code indicate error: {0}")]
    Status(u16),
    #[error("underlying transport (maybe TLS) failed")]
    Transport,
    #[error("response body not valid JSON: {0:?}")]
    Decode(#[from] serde_json::Error),
    #[error("response body indicate failure: {0}")]
    Application(serde_json::Value),
}
impl From<ureq::Error> for FetchError {
    fn from(value: ureq::Error) -> Self {
        match value {
            ureq::Error::Status(code, _) => Self::Status(code),
            ureq::Error::Transport(_) => Self::Transport,
        }
    }
}

pub type FetchResult<T> = Result<T, FetchError>;
