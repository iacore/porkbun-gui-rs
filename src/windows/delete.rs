//! Simple status window, showing whether a request succeded or not

use eframe::egui;

use crate::prelude::*;

pub struct DeleteWindow {
    record_id: u32,
    open: bool,
    task: Task<FetchResult<DeleteResponse>>,
}

impl DeleteWindow {
    pub(crate) fn new(g: &mut Globals, record_id: u32) -> Self {
        Self {
            record_id,
            open: true,
            task: Task::spawn(g.delete(record_id)),
        }
    }
}

impl TopLevelWindow for DeleteWindow {
    fn open(&self) -> bool {
        self.open
    }

    fn show(
        &mut self,
        _g: &mut Globals,
        ctx: &egui::Context,
        _frame: &mut eframe::Frame,
    ) -> CommandQueue {
        let commands = CommandQueue::new();
        egui::Window::new(format!("Delete Record {}", self.record_id))
            .open(&mut self.open)
            .show(ctx, |ui| {
                self.task.show_if_success(ui, |ui, _| {
                    ui.label("Success");
                });
            });
        commands
    }
}
