use eframe::egui;

use crate::prelude::*;

use super::EditWindow;

pub struct ReadAllWindow {
    id: u16,
    open: bool,
    task: Task<FetchResult<ReadAllResponse>>,
    selected_id: Option<u32>,
}

impl ReadAllWindow {
    pub fn new(id: u16, g: &Globals) -> Self {
        Self {
            id,
            open: true,
            task: Task::spawn(g.read_all()),
            selected_id: None,
        }
    }
}

impl TopLevelWindow for ReadAllWindow {
    fn open(&self) -> bool {
        self.open
    }

    fn show(
        &mut self,
        g: &mut Globals,
        ctx: &egui::Context,
        _frame: &mut eframe::Frame,
    ) -> CommandQueue {
        let mut commands = CommandQueue::new();
        egui::Window::new(format!("All Records #{}", self.id))
            .open(&mut self.open)
            .show(ctx, |ui| {
                let mut need_refresh = false;
                self.task
                    .show_if_success(ui, |ui, ReadAllResponse { records }| {
                        ui.horizontal(|ui| {
                            need_refresh =
                                action_bar(ui, records, self.selected_id, g, &mut commands);
                        });
                        let selected_id = &mut self.selected_id;
                        egui::ScrollArea::both().show(ui, |ui| {
                            records_table(ui, records, selected_id);
                        });
                    });
                if need_refresh {
                    self.task = Task::spawn(g.read_all());
                }
            });
        commands
    }
}

fn action_bar(
    ui: &mut egui::Ui,
    records: &[DnsRecord],
    selected_id: Option<u32>,
    g: &mut Globals,
    commands: &mut std::collections::LinkedList<AppCommand>,
) -> bool {
    let mut need_refresh = false;

    if records.is_empty() {
        ui.label("No records");
    } else if let Some(id) = selected_id {
        if ui.button("Edit").clicked() {
            let record = get_record(records, id).clone();
            let window = EditWindow::new(g, record);
            commands.create_window(window);
        }
        if ui.button("Delete").clicked() {
            let record = get_record(records, id);
            commands.create_window(DeleteWindow::new(g, record.id));
        }
    } else {
        ui.label("Click on an id number to modify the corresponding record.");
    }

    if ui.button("Refresh Page").clicked() {
        need_refresh = true;
    }

    need_refresh
}

/// all records
fn records_table(ui: &mut egui::Ui, records: &[DnsRecord], selected_id: &mut Option<u32>) {
    egui::Grid::new("").num_columns(7).show(ui, |ui| {
        records_table_header(ui);

        for record in records {
            let selected = selected_id.map_or(false, |id| record.id == id);
            if ui
                .selectable_label(selected, record.id.to_string())
                .clicked()
            {
                *selected_id = Some(record.id);
            }

            ui.label(record.name.as_deref().unwrap_or("N/A"));
            ui.label(&record.type_);
            ui.label(&record.content);
            ui.label(record.ttl.map_or("N/A".into(), |x| x.to_string()));
            ui.label(record.prio.map_or("N/A".into(), |x| x.to_string()));
            ui.label(record.notes.as_deref().unwrap_or("N/A"));
            ui.end_row();
        }

        records_table_header(ui);
    });
}

fn records_table_header(ui: &mut egui::Ui) {
    ui.vertical_centered(|ui| ui.strong("id"));
    ui.vertical_centered(|ui| ui.strong("name"));
    ui.vertical_centered(|ui| ui.strong("type"));
    ui.vertical_centered(|ui| ui.strong("content"));
    ui.vertical_centered(|ui| ui.strong("ttl"));
    ui.vertical_centered(|ui| ui.strong("prio"));
    ui.vertical_centered(|ui| ui.strong("notes"));
    ui.end_row();
}

fn get_record(records: &[DnsRecord], id: u32) -> &DnsRecord {
    records
        .iter()
        .find(|x| x.id == id)
        .expect("just got the id")
}
