use eframe::egui;

use crate::prelude::*;

pub struct CreateWindow {
    id: u16,
    open: bool,
    task: Option<Task<FetchResult<CreateResponse>>>,
    record: DnsRecord,
}

impl CreateWindow {
    pub fn new(id: u16) -> Self {
        Self {
            id,
            open: true,
            task: None,
            record: DnsRecord::default(),
        }
    }
}

impl TopLevelWindow for CreateWindow {
    fn open(&self) -> bool {
        self.open
    }

    fn show(
        &mut self,
        g: &mut Globals,
        ctx: &egui::Context,
        _frame: &mut eframe::Frame,
    ) -> CommandQueue {
        egui::Window::new(format!("Create Record #{}", self.id))
            .open(&mut self.open)
            .show(ctx, |ui| {
                let record = &mut self.record;
                record_editor(ui, record);

                if ui.button("Submit").clicked() {
                    self.task = Some(Task::spawn(g.create(&self.record)));
                }

                if let Some(task) = &mut self.task {
                    ui.separator();
                    task.show_if_success(ui, |ui, CreateResponse { id }| {
                        ui.label(format!("Success: new record id is {id}"));
                    });
                }
            });
        Default::default()
    }
}
