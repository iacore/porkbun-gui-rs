use eframe::egui;

use crate::prelude::*;

pub struct EditWindow {
    open: bool,
    task: Option<Task<FetchResult<EditResponse>>>,
    record: DnsRecord,
}

impl EditWindow {
    pub fn new(_g: &mut Globals, record: DnsRecord) -> Self {
        Self {
            open: true,
            task: None,
            record,
        }
    }
}

impl TopLevelWindow for EditWindow {
    fn open(&self) -> bool {
        self.open
    }

    fn show(
        &mut self,
        g: &mut Globals,
        ctx: &egui::Context,
        _frame: &mut eframe::Frame,
    ) -> CommandQueue {
        egui::Window::new(format!("Edit Record {}", self.record.id))
            .open(&mut self.open)
            .show(ctx, |ui| {
                let record = &mut self.record;
                record_editor(ui, record);

                if ui.button("Submit").clicked() {
                    self.task = Some(Task::spawn(g.edit(&self.record)));
                }

                if let Some(task) = &mut self.task {
                    ui.separator();
                    task.show_if_success(ui, |ui, _| {
                        ui.label("Success");
                    });
                }
            });
        Default::default()
    }
}
