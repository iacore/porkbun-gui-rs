![Screenshot](https://img.itch.zone/aW1nLzEwOTk5MDY5LnBuZw==/original/p8S85%2B.png)

A GUI for managing your DNS records on porkbun.

See [about](about.md) for usage and configuration.

---

To install, [install Rust](https://rustup.rs/) and use cargo:

```
cargo install --git https://git.envs.net/iacore/porkbun-gui-rs
```

Or, download and install

```
git clone https://git.envs.net/iacore/porkbun-gui-rs
cd porkbun-gui-rs
cargo install --path .
```
